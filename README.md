# rays-indifocuser
A relatively simple diy telescope focuser driver with a simple backlash adjustment with serial controls.

This focuser was originally based on an older version of Robert Brown's focuser-pro and has been modified to include code from mltple sources with a single backlash setting and serial commands to control it. 

Hardware: 
  I used a drv8825 driver board mounted directly to an arduino nano with a pin header. The motor output is powered by the 12v battery and a single 4 wire cable plugs into stepper motors on several different telescope OTA's.

  Switching to a TMC2208 looks to allow single powered operation using a powered usb hub, but be careful of motor requirements. The jyb units work much better with a 12v source. I bought a 12v input supply usb hub and modified it to also allow 12v output to drive servo boards and have one usb/power wire to the OTA mounted control box. <--At your peril. Serious DIY YMMV. Don't blame me if you cook things.

todo: Get indi/ekos to accept backlash from moonlite compatable controllers, or convert the driver to a standalone one.
